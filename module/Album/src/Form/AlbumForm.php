<?php
namespace Album\Form;

use Zend\Form\Form;

class AlbumForm extends Form
{
    public function __construct($name = null)
    {
        // We will ignore the name provided to the constructor
        parent::__construct('album');

        // input hidden id
        $this->add(['name' => 'id', 'type' => 'hidden',]);

        // title
        $this->add(['name' => 'title', 'type' => 'text', 'options' => ['label' => 'Title']]);
        
        // artist
        $this->add(['name' => 'artist', 'type' => 'text', 'options' => ['label' => 'Artist']]);

        // button submit
        $this->add(['name' => 'submit', 'type' => 'submit', 'attributes' => ['value' => 'Go', 'id' => 'submitbutton']]);
    }
}